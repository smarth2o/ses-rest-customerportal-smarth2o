#input user_id
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

	
Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null   

query = "SELECT b.address, d.zipcode, d.country, h.residency_type, case when h.ownership is null then null else case h.ownership when 0 then \"false\" else \"true\" end end, " +
		"case when h.second is null then null else case h.second when 0 then \"false\" else \"true\" end end, h.household_size, " +
		"b.building_size, b.age, h.number_bathrooms, case when h.household_pool is null then null else case h.household_pool when 0 then \"false\" else \"true\" end end, " +
		"h.household_pool_volume, case when h.household_garden is null then null else case h.household_garden when 0 then \"false\" when 1 then \"true\" end end, h.household_garden_area, " +
		"case when h.irrigation_system is null then null else case h.irrigation_system when 0 then \"false\" else \"true\" end end, " +
		"case when h.house_plants is null then null else case h.house_plants when 0 then \"false\" else \"true\" end end ," +
		"case when h.balcony_plants is null then null else case h.balcony_plants when 0 then \"false\" else \"true\" end end ," +
		"case when h.balcony_irrigation is null then null else case h.balcony_irrigation when 0 then \"false\" else \"true\" end end " +
		"FROM user u " +   
        "LEFT OUTER JOIN neutral_user nu ON u.oid = nu.user_oid " +    
        "LEFT OUTER JOIN household h ON nu.household_oid = h.oid " +
        "LEFT OUTER JOIN building b ON b.oid = h.building_oid " +
        "LEFT OUTER JOIN district d ON b.district_oid = d.oid " +
		"WHERE nu.user_oid = " + user_id

//println query

def result=null
def jsonString = null

def ris=0
result = session.createSQLQuery(query).list()
def average = 0;

try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
    jGenerator.writeStartArray()
     
    for (r in result){
    	//write json
    	jGenerator.writeStartObject()
	    jGenerator.writeStringField("address", r[0])
	    jGenerator.writeStringField("zipcode", r[1])
	    jGenerator.writeStringField("country", r[2])
	    jGenerator.writeStringField("residency_type", r[3])
	    jGenerator.writeStringField("owner", r[4])
	    jGenerator.writeStringField("second_home", r[5])
	    jGenerator.writeNumberField("residency_size", r[6])
	    jGenerator.writeNumberField("building_size", r[7])
	    jGenerator.writeNumberField("age", r[8])
		jGenerator.writeNumberField("number_bathrooms", r[9]!=null?new Integer(r[9]).intValue():null)
		jGenerator.writeStringField("pool", r[10])
	    jGenerator.writeNumberField("pool_size", r[11])
		jGenerator.writeStringField("garden", r[12])
	    jGenerator.writeNumberField("garden_size", r[13])	    
	    jGenerator.writeStringField("irrigation", r[14])
	    jGenerator.writeStringField("house_plants", r[15])
	    jGenerator.writeStringField("balcony_plants", r[16])
	    jGenerator.writeStringField("balcony_irrigation", r[17])
	    jGenerator.writeEndObject()
	}

	jGenerator.writeEndArray()
    jGenerator.close()

    json = out.toString("UTF-8")

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

commit(session)

//println json
ArrayNode mapper = new ObjectMapper().readTree(json);
//println mapper
return ["json" : mapper]