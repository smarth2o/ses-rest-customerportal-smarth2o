#input user_id
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

	
Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null

query = "select (max(mr.total_consumption_adjusted)-min(mr.total_consumption_adjusted)) as monthlyTotalConsumption " +
		"from user u " +
        "left outer join neutral_user nu on u.oid = nu.user_oid " +  
        "left outer join household h on nu.household_oid = h.oid " +
        "left outer join building b on b.oid = h.building_oid " +
		"left outer join smart_meter sm on sm.oid = h.smart_meter_oid " +  
		"left outer join meter_reading mr on mr.smart_meter_oid = sm.oid " +
        "where nu.user_oid = " + user_id + " and " +
        "MONTH(mr.reading_date_time) = MONTH(NOW())"
        
//println query

def result=null
def jsonString = null

def ris=0
result = session.createSQLQuery(query).list()
def average = 0;

try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
    jGenerator.writeStartArray()


    for (r in result){
    	if(r!=null)
	    {
	    	//write json
	    	jGenerator.writeStartObject()
	    	jGenerator.writeNumberField("monthlyTotalConsumption", Double.parseDouble(r.toString()))
		    jGenerator.writeEndObject()
		 }
		 else
		 {
			 jGenerator.writeStartObject()
			 jGenerator.writeNumberField("monthlyTotalConsumption", -1)
			 jGenerator.writeEndObject()
		 }
	}

	jGenerator.writeEndArray()
    jGenerator.close()

    json = out.toString("UTF-8")

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

commit(session)

//println json
ArrayNode mapper = new ObjectMapper().readTree(json);
//println mapper
return ["json" : mapper]