#input user_id
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

	
Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null   

query = "SELECT b.total_consumption, b.year " +
		"FROM user u " +   
        "LEFT OUTER JOIN neutral_user nu ON u.oid = nu.user_oid " +    
        "LEFT OUTER JOIN household h ON nu.household_oid = h.oid " +
        "LEFT OUTER JOIN smart_meter sm ON h.smart_meter_oid = sm.oid " +
        "LEFT OUTER JOIN baseline b ON sm.oid = b.smart_meter_oid " +
		"WHERE nu.user_oid = " + user_id + " " +
		"ORDER BY b.year DESC LIMIT 1"

//println query

def result=null
def jsonString = null

def year=0
def days=365
def size=0
def ndnu=false

result = session.createSQLQuery(query).list()

size =  result.size()

try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    // write to file
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
    jGenerator.writeStartArray()
        
    //write json
    jGenerator.writeStartObject()
    
    if (size != 0)    {
    
      if (result.get(0)[0] != null)
     
    	for (r in result){
    	
     	   // extracting the year
     	   year = Integer.parseInt(r[1].toString().substring(0,4))
     	   
      	   // if bisect
      	   if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))  
      	   { 
      	   		days = 366 
      	   }

       	   jGenerator.writeNumberField("month", r[0]/12)
           jGenerator.writeNumberField("week", r[0]/52)
           jGenerator.writeNumberField("day", r[0]/days)
      
         }
       else { ndnu = true } 
    }
    else { ndnu = true }
    
    // if no data or no user
    if (ndnu) {
       jGenerator.writeStringField("month", "")
       jGenerator.writeStringField("week", "")
       jGenerator.writeStringField("day", "")
    }
    
    jGenerator.writeEndObject()
      
    jGenerator.writeEndArray()
    jGenerator.close()

    json = out.toString("UTF-8")
    //println json
   

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

return ["json" : json]
