#input oid
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

println oid
String[] oid = oid
println oid

Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null

OutputStream out = new ByteArrayOutputStream()

JsonFactory jfactory = new JsonFactory();

/*** write to file ***/
JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
jGenerator.writeStartArray()

jGenerator.writeStartObject()
jGenerator.writeStringField("setHouseholdInfo", "true")
jGenerator.writeEndObject()

jGenerator.writeEndArray()
jGenerator.close()

json = out.toString("UTF-8")

println json
ArrayNode mapper = new ObjectMapper().readTree(json);
println mapper
return ["json" : mapper]