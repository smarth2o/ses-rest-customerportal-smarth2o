#input user_id
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

	
Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null

       
query = "select  " +
		"monthly_filtered.monthly_consumption,  " +
		"weekly_filtered.weekly_consumption,  " +
		"daily_filtered.daily_consumption  " +
		"from  " +
		
		"(select sum(daily.daily_avg_consumption*daily.days)/sum(daily.days) daily_consumption from  " +
		"(select  h.smart_meter_oid, (max(mr.total_consumption_adjusted)-min(mr.total_consumption_adjusted))/   " +
		"DATEDIFF(max(date(mr.reading_date_time)), min(date(mr.reading_date_time))) as daily_avg_consumption,   " +
		"DATEDIFF(max(date(mr.reading_date_time)), min(date(mr.reading_date_time))) days  " +
		"from neutral_user nu    " +
        "left outer join household h on nu.household_oid = h.oid   " +
        "left outer join building b on b.oid = h.building_oid   " +
        "left outer join district d on d.oid = b.district_oid   " +
		"left outer join meter_reading mr on mr.smart_meter_oid = h.smart_meter_oid   " +
        "where d.zipcode = (select d.zipcode from neutral_user nu     " +
        "left outer join household h on nu.household_oid = h.oid   " +
        "left outer join building b on b.oid = h.building_oid   " +
        "left outer join district d on d.oid = b.district_oid   " +
        " where nu.user_oid = " +user_id +") " + 
        "group by mr.smart_meter_oid) daily  " +
 		"where  daily.daily_avg_consumption != 0 and   " +
		"daily.daily_avg_consumption is not null) daily_filtered,  " +

        "(select sum(weekly.weekly_avg_consumption*weekly.weeks)/sum(weekly.weeks) weekly_consumption from  " +
		"(select  h.smart_meter_oid, (max(mr.total_consumption_adjusted)-min(mr.total_consumption_adjusted))/  " +
		"FLOOR(DATEDIFF(max(date(mr.reading_date_time)), min(date(mr.reading_date_time)))/7) as weekly_avg_consumption,  " +
		"FLOOR(DATEDIFF(max(date(mr.reading_date_time)), min(date(mr.reading_date_time)))/7) as weeks  " +
		"from neutral_user nu  " +
        "left outer join household h on nu.household_oid = h.oid  " +
        "left outer join building b on b.oid = h.building_oid  " +
        "left outer join district d on d.oid = b.district_oid  " +
		"left outer join meter_reading mr on mr.smart_meter_oid = h.smart_meter_oid 	 " +
        "where d.zipcode = (select d.zipcode from neutral_user nu  " +
        "left outer join household h on nu.household_oid = h.oid  " +
        "left outer join building b on b.oid = h.building_oid  " +
        "left outer join district d on d.oid = b.district_oid    " +
        " where nu.user_oid = " +user_id +") " +   
        "group by mr.smart_meter_oid) weekly  " +
        "where  weekly.weekly_avg_consumption != 0 and   " +
		"weekly.weekly_avg_consumption is not null) weekly_filtered,  " +

		"(select  sum(monthly.monthly_avg_consumption*monthly.months)/sum(monthly.months) monthly_consumption from  " +
		"(select h.smart_meter_oid, (max(mr.total_consumption_adjusted)-min(mr.total_consumption_adjusted))/  " +
		"PERIOD_DIFF(date_format(max(mr.reading_date_time), '%Y%m'), date_format(min(mr.reading_date_time), '%Y%m')) as monthly_avg_consumption,  " +
		"PERIOD_DIFF(date_format(max(mr.reading_date_time), '%Y%m'), date_format(min(mr.reading_date_time), '%Y%m')) as months  " +
		"from neutral_user nu    " +
        "left outer join household h on nu.household_oid = h.oid  " +
        "left outer join building b on b.oid = h.building_oid  " +
        "left outer join district d on d.oid = b.district_oid  " +
		"left outer join meter_reading mr on mr.smart_meter_oid = h.smart_meter_oid 	 " +
        "where d.zipcode = (select d.zipcode from neutral_user nu  " +
        "left outer join household h on nu.household_oid = h.oid  " +
        "left outer join building b on b.oid = h.building_oid  " +
        "left outer join district d on d.oid = b.district_oid  " +
        " where nu.user_oid = " +user_id +") " +  
        "group by mr.smart_meter_oid) monthly  " +
        "where  monthly.monthly_avg_consumption != 0 and   " +
		"monthly.monthly_avg_consumption is not null) monthly_filtered    "
		      

println query

def result=null
def jsonString = null

def ris=0
result = session.createSQLQuery(query).list()
def average = 0;

try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
    jGenerator.writeStartArray()


    for (r in result){
	    if(r[0]!=null)
	    {
	    	//write json
	    	jGenerator.writeStartObject()
		    jGenerator.writeNumberField("month_avg", new Double(r[0]).doubleValue())
		    jGenerator.writeNumberField("week_avg", new Double(r[1]).doubleValue())
		    jGenerator.writeNumberField("day_avg", new Double(r[2]).doubleValue())
		    jGenerator.writeEndObject()
	    }
	    else
	    {
	    	jGenerator.writeStartObject()
	    	jGenerator.writeNumberField("month_avg", -1)
		    jGenerator.writeNumberField("week_avg", -1)
		    jGenerator.writeNumberField("day_avg", -1)
	    	jGenerator.writeEndObject()
	    }
	}

	jGenerator.writeEndArray()
    jGenerator.close()

    json = out.toString("UTF-8")

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

commit(session)

//println json
ArrayNode mapper = new ObjectMapper().readTree(json);
//println mapper
return ["json" : mapper]