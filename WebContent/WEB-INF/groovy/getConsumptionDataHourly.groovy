#input user_id
#input data
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null

query = /*"SELECT max(m.total_consumption_adjusted) qty, max(m.reading_date_time) tmstp" +
		" FROM meter_reading m  " +
		" LEFT OUTER JOIN smart_meter sm ON sm.oid = m.smart_meter_oid " +
		" LEFT OUTER JOIN household h ON sm.oid = h.smart_meter_oid " +
		" LEFT OUTER JOIN building b ON b.oid = h.building_oid " +
		" LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
        " LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		" WHERE u.oid = " + user_id + " AND DATE(m.reading_date_time) = " +
        " (SELECT DATE(MAX(m.reading_date_time)) " +
        " FROM meter_reading m " +
        " 		LEFT OUTER JOIN smart_meter sm ON sm.oid = m.smart_meter_oid " + 
        " 		LEFT OUTER JOIN household h ON sm.oid = h.smart_meter_oid " +
        " 		LEFT OUTER JOIN building b ON b.oid = h.building_oid " +
        " 		LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
        "         LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
        " 		WHERE u.oid = " + user_id + ") " +
        " GROUP BY m.oid " +
        " ORDER BY m.reading_date_time"*/
        
        
        "SELECT hourly_consumption.qty, max(hourly_consumption.tmstp) " +
        " FROM " +
        " (SELECT T1.oid, T1.min_cons - T2.max_cons qty,  T1.min_time tmstp" +
		" FROM " +
    	" (SELECT @curRow1 ::= @curRow1 + 1 min_row_nr, m.oid, m.total_consumption_adjusted as min_cons, CONVERT_TZ(m.reading_date_time,'GMT','CET') as min_time " +
		" FROM meter_reading m " +
		" LEFT OUTER JOIN smart_meter sm ON sm.oid = m.smart_meter_oid " +
		" LEFT OUTER JOIN household h ON sm.oid = h.smart_meter_oid " +
		" LEFT OUTER JOIN building b ON b.oid = h.building_oid " +
		" LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
        " LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
        " JOIN (SELECT @curRow1 ::= 0) r " + 
		" WHERE u.oid =   " + user_id + " and (date(CONVERT_TZ(m.reading_date_time,'GMT','CET')) = \'" + data + "\' or CONVERT_TZ(m.reading_date_time,'GMT','CET') = DATE_ADD(\'" + data + "\', INTERVAL 24 HOUR))" +
        " GROUP BY m.oid " +
        " ORDER BY m.reading_date_time desc limit 25) T1 INNER JOIN " +
         
        "(SELECT @curRow2 ::= @curRow2 + 1 max_row_nr, m.oid, m.total_consumption_adjusted as max_cons, CONVERT_TZ(m.reading_date_time,'GMT','CET') as max_time " +
		" FROM meter_reading m  " +
		" LEFT OUTER JOIN smart_meter sm ON sm.oid = m.smart_meter_oid " +
		" LEFT OUTER JOIN household h ON sm.oid = h.smart_meter_oid " +
		" LEFT OUTER JOIN building b ON b.oid = h.building_oid " +
		" LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
        " LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
        " JOIN (SELECT @curRow2 ::= 0) r " +
		" WHERE u.oid =   " + user_id + " and (date(CONVERT_TZ(m.reading_date_time,'GMT','CET')) = \'" + data + "\' or CONVERT_TZ(m.reading_date_time,'GMT','CET') = DATE_ADD(\'" + data + "\', INTERVAL 24 HOUR))" +
        " GROUP BY m.oid " +
        " ORDER BY m.reading_date_time desc limit 25) T2 on T2.max_row_nr - 1 = T1.min_row_nr  ) hourly_consumption " +
        " GROUP BY hourly_consumption.oid " +
        " ORDER BY hourly_consumption.tmstp"

println query

def result=null
def jsonString = null

def ris=0
result = session.createSQLQuery(query).list()
def average = 0;

try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
    jGenerator.writeStartArray()


    for (r in result){
    	//write json
    	jGenerator.writeStartObject()
	    jGenerator.writeNumberField("quantity", r[0]!=null?new Double(r[0]).doubleValue():0.0)
	 	jGenerator.writeStringField("timestamp", r[1].toString())
	    jGenerator.writeEndObject()

	}

	jGenerator.writeEndArray()
    jGenerator.close()

    json = out.toString("UTF-8")

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

commit(session)

println json
ArrayNode mapper = new ObjectMapper().readTree(json);
println mapper
return ["json" : mapper]